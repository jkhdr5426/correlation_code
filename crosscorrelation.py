import numpy as np
import matplotlib.pyplot as plt

def cc(x, y):
    assert len(x) == len(y), "Input signals must have the same length"
    N = len(x)
    result = np.correlate(x, y, mode='full') / np.sqrt(np.sum(x**2) * np.sum(y**2))
    lags = np.arange(-N + 1, N)
    return lags, result