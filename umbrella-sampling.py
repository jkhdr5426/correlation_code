import openmm as mm
from openmm import unit
import numpy as np
import matplotlib.pyplot as plt
import mdtraj as md

# Load PDB file
pdb_file = '/Users/edwardkim/Desktop/Educatory/CS/VSC/personal/igem_openmm/software_code/pdbfiles/ahl_dock_luxr_1.pdb'
traj = md.load(pdb_file)

# Define a one-dimensional potential energy function for a polynomial (modify as needed)
def potential_function(x, coefficients):
    terms = [coeff * (x - 3)**i for i, coeff in enumerate(coefficients)]
    return sum(terms)

# Define umbrella sampling parameters
num_steps = 30000
num_windows = 20
spring_constant = 2.0  # Strength of harmonic biasing potential

# Coefficients for the polynomial potential energy function
polynomial_coefficients = [0.25, 0, -2, 0, 0.5]

# Create OpenMM system
system = mm.System()
for atom in traj.topology.atoms:
    system.addParticle(atom.element.mass)

# Create harmonic biasing force with a polynomial potential
force_expression = 'A*(' + '+'.join([f'(x-B{i})^{i}' for i in range(len(polynomial_coefficients))]) + ')'
force = mm.CustomExternalForce(force_expression)
force.addGlobalParameter('A', spring_constant * unit.kilocalories_per_mole / unit.angstroms**2)

for i, coeff in enumerate(polynomial_coefficients):
    force.addGlobalParameter(f'B{i}', coeff)

for atom in traj.topology.atoms:
    force.addParticle(atom.index, [])  # Apply force to all particles in the system

system.addForce(force)

# Set up Langevin integrator
integrator = mm.LangevinIntegrator(300 * unit.kelvin, 0.1 / unit.picoseconds, 0.002 * unit.picoseconds)

# Create context
platform = mm.Platform.getPlatformByName('CPU')  # Change to 'OpenCL' or 'CUDA' if available
context = mm.Context(system, integrator, platform)


# Perform umbrella sampling
positions = np.zeros((num_windows, num_steps))
energies = np.zeros((num_windows, num_steps))

for window in range(num_windows):
    # Set the biasing potential parameters for the current window
    for i, coeff in enumerate(polynomial_coefficients):
        context.setParameter(f'B{i}', coeff)

    # Set initial positions from the PDB file
    context.setPositions(traj.xyz[0])

    # Run the simulation
    for step in range(num_steps):
        integrator.step(1)
        state = context.getState(getPositions=True, getEnergy=True)
        positions[window, step] = state.getPositions(asNumpy=True).value_in_unit(unit.angstroms).reshape(-1)[0]
        energies[window, step] = state.getPotentialEnergy() / unit.kilocalories_per_mole
        if step % 1000 == 0:
            print(f"Step {step}, Position: {positions[window, step]}, Energy: {energies[window, step]}")

for window in range(num_windows):
    plt.plot(positions[window, :], energies[window, :], label=f'Window {window + 1}')

plt.title('Umbrella Sampling along Reaction Coordinate')
plt.xlabel('Reaction Coordinate (Å)')
plt.ylabel('Potential of Mean Force (kcal/mol)')
# Set y-axis limits to focus on a specific energy range
plt.ylim(min(energies.flatten()) - 1, max(energies.flatten()) + 1)
plt.legend()
plt.show()
