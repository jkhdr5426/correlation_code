import numpy as np

def circular_cross_correlation(x, y):
    N = len(x)
    result = np.fft.ifft(np.fft.fft(x) * np.conj(np.fft.fft(y)))
    return np.roll(result, N // 2)
